/* eslint-disable prettier/prettier */
import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Res, NotFoundException, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './interfaces/user.interface';
import { CreateUserDto } from './dto/create-user-dto';
import { genSalt, hash } from 'bcrypt';

import { JwtAuthGuard } from '../auth/shared/jwt-auth.guard';

@Controller('user')
export class UserController {

    constructor(private readonly _userService: UserService) {

    }

    @UseGuards(JwtAuthGuard)
    @Get('/')
    async getUsers(@Res() res): Promise<User[]> {
        const user = await this._userService.getUsers();
        return res.status(HttpStatus.OK).json({ user: user });
    }

    @UseGuards(JwtAuthGuard)
    @Get('/:id')
    async getUser(@Res() res, @Param('id') id: string): Promise<User> {

        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            throw new NotFoundException('Id Not Valid..')
        }
        const user = await this._userService.getUser(id);
        if (!user) throw new NotFoundException('User Does not exists')
        return res.status(HttpStatus.OK).json({ user: user });
    }

    @Post('/create')
    async createUser(@Res() res, @Body() createUserDto: CreateUserDto): Promise<User> {

        const salt = await genSalt(10);
        createUserDto.password = await hash(createUserDto.password, salt);

        const user = await this._userService.createUser(createUserDto);
        return res.status(HttpStatus.OK).json({ message: 'User Create ..', user: user });
    }

    @UseGuards(JwtAuthGuard)
    @Put('/update/:id')
    async update(@Res() res, @Param('id') id: string, @Body() createUserDto: CreateUserDto): Promise<User> {
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            throw new NotFoundException('Id Not Valid..')
        }
        const user = await this._userService.updateUser(id, createUserDto);
        if (!user) throw new NotFoundException('User Does not exists')

        return res.status(HttpStatus.OK).json({ message: 'User Update ..', user: user });

    }

    @UseGuards(JwtAuthGuard)
    @Delete('/delete/:id')
    async delete(@Res() res, @Param('id') id: string): Promise<User> {
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            throw new NotFoundException('Id Not Valid..')
        }
        const user = await this._userService.deleteUser(id);
        if (!user) {
            throw new NotFoundException('User Does not exists')
        }
        return res.status(HttpStatus.OK).json({ message: 'User deleted ..', user: user });

    }
}
