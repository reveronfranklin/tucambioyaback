/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { User } from './interfaces/user.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user-dto';


@Injectable()
export class UserService {

    constructor(@InjectModel('User') private readonly userModel: Model<User>) {

    }

    async getUsers(): Promise<User[]> {
        const users = await this.userModel.find();
        return users;
    }

    async getUser(userId: string): Promise<User> {

        const users = await this.userModel.findById(userId);
        return users;
    }
    async getUserByEmail(email: string): Promise<User> {

        const users = await this.userModel.findOne({ email });
        return users;
    }

    async createUser(user: CreateUserDto): Promise<User> {
        const userCreated = new this.userModel(user);
        await userCreated.save();
        return userCreated;
    }

    async deleteUser(id: string): Promise<User> {
        const deletedUser = await this.userModel.findByIdAndDelete(id);
        return deletedUser;
    }

    async updateUser(id: string, user: CreateUserDto): Promise<User> {
        const updatedUser = await this.userModel.findByIdAndUpdate(id, user, { new: true });
        return updatedUser;
    }



}
