/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';

export interface User extends Document {
    _id?: string;
    email: string;
    password: string;
    name: string;
    lastName: string;
    active: boolean;
}