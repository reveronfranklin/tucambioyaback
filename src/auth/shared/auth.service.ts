/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { UserService } from '../../user/user.service';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
@Injectable()
export class AuthService {

    constructor(private _userService: UserService,
        private jwtService: JwtService) { }


    async validateUser(userEmail: string, userpassword: string) {
        const user = await this._userService.getUserByEmail(userEmail);

        const isMatch = await compare(userpassword, user.password);

        if (user && isMatch) {
            const { _id, name, email } = user;
            return { id: _id, name, email };
        }
        return null;
    }

    async login(user: any) {

        const payload = { email: user.email, sub: user.id };
        return {
            access_token: this.jwtService.sign(payload),
        };

    }

}
