/* eslint-disable prettier/prettier */
export class CreateUserDto {
    _id: string;
    email: string;
    password: string;
    name: string;
    lastName: string;
    active: boolean;

}