/* eslint-disable prettier/prettier */
import { AuthModule } from './auth/auth.module';


import { Module } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { Configuration } from './config/config.jey';
import { UserModule } from './user/user.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    AuthModule,
    ConfigModule,
    UserModule,
    MongooseModule.forRoot(`mongodb://localhost:27017/tucambioya`, {
      useNewUrlParser: true,
      userUnifiedTopology: true
    })
  ],

})
export class AppModule {

  static port: number | string;
  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get(Configuration.PORT);
  }
}
